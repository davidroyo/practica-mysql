<?php
require_once 'controllers/dbController.php';
require_once 'models/db.php';
session_start();

$dbController = new dbController();
$databases = $dbController->getDatabases();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Practica 1</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="right">
        <div class="filtros">
            <h4>Filtrar por:</h4>
            <button type="button" name="button">Favoritos</button>
            <button type="button" name="button">Executed_at</button>
        </div>
        <div class="history">
            <?php
            if(is_array($_SESSION['history']))
            {
                // Recorrèm les files que hem guardat a session desde dbController.php
                foreach($_SESSION['history'] as $key=>$value)
                {
                    $queryId = '';
                    echo "<form action='favoritosForm.php' method='post'><pre>";

                    foreach($value as $key=>$value2)
                    {
                        if($key == 'id')
                        {
                            $queryId = $value2;
                        }
                        echo "<b>$key</b>: $value2 ";
                    }

                    echo "</pre><input hidden type='text' name='idQuery' value='$queryId'>";
                    echo "<button type='submit' name='favorito' value=''>Favorito</button>";
                    echo '</form><hr>';
                }
            }else{
                echo '<pre>No hay historial de consultas</pre>';
            }
            ?>

        </div>
    </div>
    <div class="content">

        <div class="left">
            <h3>Ejecutar consulta</h3>
            <form name="form" action="form.php" method="post">

                <?php
                // Creem element select
                echo '<select name="database">';
                // Creem cada element option per cada valor obtingut
                foreach($databases as $key=>$value)
                {
                    if ($value == 'practica_mysql')
                    {
                        echo "<option selected='selected' value='$value'>$value</option>";
                    }else
                    {
                        echo "<option value='$value'>$value</option>";
                    }
                    // Agafem el nom de la bd que hem obtingut
                    // echo $databases[$key];
                }
                echo '</select>';
                ?>

                <textarea name="query" id="" cols="30" rows="10"></textarea>
                <button type="submit" name="submit">Enviar consulta</button>
            </form>
            <div class="results">
                <?php
                if(is_array($_SESSION['result'])){
                    // Recorrèm les files que hem guardat a session desde dbController.php
                    foreach($_SESSION['result'] as $key=>$value) {
                        foreach($value as $key=>$value2) {
                            echo "$key=$value2;<br>";
                        }
                        echo '<hr>';
                    }
                }else{
                    echo '<pre>'.$_SESSION['result'].'</pre>';
                }
                ?>
            </div>
        </div>
    </div>
</body>
</html>
<?php
$_SESSION['result'] = '';
// $_SESSION['history'] = '';
?>
