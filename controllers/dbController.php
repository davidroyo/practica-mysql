<?php
require_once 'models/db.php';

class dbController {
    public $db;

    public function __construct()
    {
        $this->db = db::getInstance();
    }

    public function getDatabases()
    {
        $databases = $this->db->getConnection()->query("
        SELECT schema_name
        FROM information_schema.schemata
        ");

        while($row = $databases->fetch_assoc()) {

            $resultados[] = $row['SCHEMA_NAME'];
        }
        // var_dump($resultados);
        return $resultados;

    }

    public function createTableHistory()
    {
        $sql = "
        CREATE TABLE SqlHistory (
            id integer UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            sentence VARCHAR(255) NOT NULL,
            executed_at DATETIME DEFAULT CURRENT_TIMESTAMP,
            favourite tinyint DEFAULT 0
        )
        ";

        $result = $this->db->getConnection()->query($sql);

        if(!$result)
        {
            echo "No s'ha creat la taula sqlHistory (potser ja existeix)<br>";

        }else {
            echo 'Taula sqlHistory creada<br>';
            return true;
        }

        // Ara hem de seleccionar tots els registres ordenats per executed_at
        $sql = "SELECT * FROM sqlHistory ORDER BY executed_at DESC";
        $result = $this->db->getConnection()->query($sql);

        if(!$result)
        {
            echo 'Could not get all from sqlHistory';
            return false;
        }

        // Retornem 5 si ha anat bé pero no hi han resultats
        if(is_object($result) && $result->num_rows == 0)
        {
            echo 'El historial de consultas está vacío';
            return true;
        }

        // Si la query es correcte, retornem true
        $_SESSION['history'] = array();
        while($row = $result->fetch_assoc())
        {
            $rows[] = $row;
        }
        foreach($rows as $row)
        {
            $_SESSION['history'][] = $row;
        }

        echo 'Got all from sqlHistory';
        return true;

    }

    public function selectDb($database)
    {
        $result = $this->db->getConnection()->query("USE $database");

        if(!$database)
        {
            return false;
        }

        if($result)
        {
            $this->createTableHistory();
            return true;
        }
        return false;
    }

    public function saveQuery($sqlPassed)
    {
        $sql = "
        INSERT INTO sqlhistory (id,sentence)
        VALUES (0,'$sqlPassed');
        ";

        $result = $this->db->getConnection()->query($sql);

        if(!$result)
        {
            echo 'query insertada';
            return false;
        }

        echo 'query no insertada';
    }

    public function query($sql, $database=false)
    {

        $this->selectDb($database);
        $result = $this->db->getConnection()->query($sql);

        if(!$result)
        {
            // Si la query no es correcte retornèm false
            $_SESSION['result'] = "Error en la consulta, no s'ha executat";
            return false;
        }
        // Retornem 5 si ha anat bé pero no hi han resultats
        if(is_object($result) && $result->num_rows == 0)
        {
            $_SESSION['result'] = "Consulta executada, pero sense resultats";
            echo 'Sense resultats';
            $this->saveQuery($sql);
            return true;
        }

        // Si la query es correcte, retornem true
        while($row = $result->fetch_assoc())
        {
            $rows[] = $row;
        }
        foreach($rows as $row)
        {
            $_SESSION['result'][] = $row;
        }

        $this->saveQuery($sql);

        return true;

    }


}

?>
