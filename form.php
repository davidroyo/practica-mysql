<?php
require_once 'controllers/dbController.php';
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $dbController = new dbController();
    $database = $_POST['database'];
    $query = $_POST['query'];
    $_SESSION['result'] = array();

    $result = $dbController->query($query, $database);

    header('Location: index.php');
}

?>
